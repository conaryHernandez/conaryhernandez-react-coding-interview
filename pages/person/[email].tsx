import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import {
  Button,
  PageHeader,
  Descriptions,
  Form,
  Input,
  message,
  Select,
  DatePicker,
} from 'antd';
import moment from 'moment';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

const { Option } = Select;

const dateFormat = 'YYYY/MM/DD';

const PersonDetail = () => {
  const router = useRouter();
  const [isEditMode, setIsEditMode] = useState(false);

  const userEmail = router.query?.email as string;

  const { load, loading, save, data } = usePersonInformation(userEmail, true);

  const toggleEditMode = () => setIsEditMode((prev) => !prev);

  const onFinish = (values: any) => {
    save({ ...values, birthday: values.birthday.format('YYYY-MM-DD'), email: userEmail });

    setIsEditMode(false);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={toggleEditMode}>
            {isEditMode ? 'Cancel' : 'Edit'}
          </Button>,
        ]}
      >
        {isEditMode ? <p>We are in edit mode</p> : <p>View Mode</p>}

        {data && !isEditMode ? (
          <Descriptions size="small" column={1}>
            <Descriptions.Item label="Name">{data.name}</Descriptions.Item>
            <Descriptions.Item label="Gender">{data.gender}</Descriptions.Item>
            <Descriptions.Item label="Phone">{data.phone}</Descriptions.Item>

            <Descriptions.Item label="Birthday">{data.birthday}</Descriptions.Item>
          </Descriptions>
        ) : (
          <Form
            name="basic"
            initialValues={{ ...data, birthday: moment(data.birthday, dateFormat) }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            style={{ maxWidth: '900px' }}
          >
            <Form.Item
              label="Name"
              name="name"
              rules={[{ required: true, message: 'Please input your name!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item name="gender" label="Gender" rules={[{ required: true }]}>
              <Select
                placeholder="Select a option and change input text above"
                allowClear
              >
                <Option value="male">male</Option>
                <Option value="female">female</Option>
                <Option value="other">other</Option>
              </Select>
            </Form.Item>

            <Form.Item
              label="phone"
              name="phone"
              rules={[{ required: true, message: 'Please input your phone!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item label="Birthday" name="birthday">
              <DatePicker />
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
